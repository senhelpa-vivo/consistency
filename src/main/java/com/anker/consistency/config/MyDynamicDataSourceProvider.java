/*
package com.anker.consistency.config;


import com.anker.consistency.entity.DatabaseSource;
import com.anker.consistency.service.IDatabaseSourceService;
import com.baomidou.dynamic.datasource.provider.DynamicDataSourceProvider;
import com.zaxxer.hikari.HikariDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


@Configuration
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class MyDynamicDataSourceProvider  implements DynamicDataSourceProvider {

    private final IDatabaseSourceService databaseSourceService;

    @Override
    public Map<String, DataSource> loadDataSources() {
        Map<String, DataSource> dataSourceMap = new HashMap<>();
        List<DatabaseSource> list = databaseSourceService.list();
        if (list!=null && !list.isEmpty()) {
            Map<String, DatabaseSource> databaseSourceMap = list.stream().collect(Collectors.toMap(DatabaseSource::getCode, Function.identity(), (a, b) -> a));
            databaseSourceMap.forEach((k, v) -> dataSourceMap.put(k, createDataSource(v)));
        }
        return dataSourceMap;
    }




    private DataSource createDataSource(DatabaseSource properties) {
        return DataSourceBuilder.create()
                .driverClassName("com.mysql.jdbc.Driver")
                .username(properties.getUserName())
                .password(properties.getPassWord())
                .url(properties.getUrl())
                .type(HikariDataSource.class)
                .build();
    }


}
*/
