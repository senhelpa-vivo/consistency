package com.anker.consistency.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author gl
 * @since 2023-02-22
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("database_source")
public class DatabaseSource implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String url;

    private String userName;

    private String passWord;

    private String code;

    private String databasetype;

    public DatabaseSource(String poolName, String url, String userName, String passWord) {

        this.url = url;
        this.userName = userName;
        this.passWord = passWord;
        this.code = poolName;
        this.databasetype = "mysql";
    }

}
