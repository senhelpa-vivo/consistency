package com.anker.consistency.controller;


import com.anker.consistency.entity.DataSourceDTO;
import com.anker.consistency.entity.DatabaseSource;
import com.anker.consistency.service.IDatabaseSourceService;
import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.creator.DruidDataSourceCreator;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author gl
 * @since 2023-02-22
 */
@RestController
@RequestMapping("/database-source")
public class DatabaseSourceController {



    @Autowired
    IDatabaseSourceService databaseSourceService;

    @GetMapping("/list")
    public List<DatabaseSource> list() {
        return databaseSourceService.list();
    }

    @GetMapping("/get")
    public List<DatabaseSource> query() {
        return databaseSourceService.getDatabaseSource("slave");
    }

    @GetMapping
    public Set<String> now() {
        return databaseSourceService.getInfo();

    }


    @PostMapping("/add")
    public Set<String> add(@Validated @RequestBody DataSourceDTO dto) {
        return databaseSourceService.add(dto);
    }
}
