package com.anker.consistency.service.impl;

import com.anker.consistency.aspect.MyDs;
import com.anker.consistency.entity.DataSourceDTO;
import com.anker.consistency.entity.DatabaseSource;
import com.anker.consistency.mapper.DatabaseSourceMapper;
import com.anker.consistency.service.IDatabaseSourceService;
import com.baomidou.dynamic.datasource.DynamicRoutingDataSource;
import com.baomidou.dynamic.datasource.creator.DefaultDataSourceCreator;
import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DataSourceProperty;
import com.baomidou.dynamic.datasource.toolkit.DynamicDataSourceContextHolder;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author gl
 * @since 2023-02-22
 */
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DatabaseSourceServiceImpl extends ServiceImpl<DatabaseSourceMapper, DatabaseSource> implements IDatabaseSourceService {


    private final DataSource dataSource;

    private final DefaultDataSourceCreator dataSourceCreator;


    @Autowired(required = false)
    public void putDataSourceHandler() {
        List<DatabaseSource> list = this.list();
        for (DatabaseSource databaseSource : list) {
            DataSourceProperty dataSourceProperty = new DataSourceProperty();
            dataSourceProperty.setUrl(databaseSource.getUrl());
            dataSourceProperty.setUsername(databaseSource.getUserName());
            dataSourceProperty.setPassword(databaseSource.getPassWord());
            dataSourceProperty.setDriverClassName("com.mysql.cj.jdbc.Driver");
            DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
            DataSource dataSource = dataSourceCreator.createDataSource(dataSourceProperty);
            ds.addDataSource(databaseSource.getCode(), dataSource);
        }
    }


    @Override
    public Set<String> add(DataSourceDTO dto) {
        DataSourceProperty dataSourceProperty = new DataSourceProperty();
        BeanUtils.copyProperties(dto, dataSourceProperty);
        DynamicRoutingDataSource ds = (DynamicRoutingDataSource) dataSource;
        DataSource dataSource = dataSourceCreator.createDataSource(dataSourceProperty);
        ds.addDataSource(dto.getPoolName(), dataSource);

        DatabaseSource databaseSource = new DatabaseSource(dto.getPoolName(),dto.getUrl(),dto.getUsername(),dto.getPassword());
        this.save(databaseSource);
        return  ds.getCurrentDataSources().keySet();
    }

    @Override
    public Set<String> getInfo() {
        DynamicRoutingDataSource  ds = (DynamicRoutingDataSource ) dataSource;
        return ds.getCurrentDataSources().keySet();
    }

    @MyDs(expression = "#name")
    @Override
    public List<DatabaseSource> getDatabaseSource(String name) {
        return list();
    }
}
