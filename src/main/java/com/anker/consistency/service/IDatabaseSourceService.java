package com.anker.consistency.service;

import com.anker.consistency.entity.DataSourceDTO;
import com.anker.consistency.entity.DatabaseSource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Set;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author gl
 * @since 2023-02-22
 */
public interface IDatabaseSourceService extends IService<DatabaseSource> {


    Set<String> add(DataSourceDTO dto);

    Set<String> getInfo();

    List<DatabaseSource> getDatabaseSource(String name);
}
