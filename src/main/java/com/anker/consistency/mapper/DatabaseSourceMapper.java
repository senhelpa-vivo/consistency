package com.anker.consistency.mapper;

import com.anker.consistency.entity.DatabaseSource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author gl
 * @since 2023-02-22
 */
@Mapper
public interface DatabaseSourceMapper extends BaseMapper<DatabaseSource> {

}
